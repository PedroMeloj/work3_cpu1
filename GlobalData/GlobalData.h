/*****************************************************************************/
/**
 * @file GlobalData.h
 * @brief GlobalData header file. Define global variables that contains
 *        application state machine transition flags and current measurements.
 *
 * @author Marco Vinicio (MV)
 *
 * @copyright Universidade Federal de Minas Gerais. All rights reserved
 *****************************************************************************/

#ifndef GLOBALDATA_GLOBALDATA_H_
#define GLOBALDATA_GLOBALDATA_H_

#include <stdbool.h>

typedef struct{

    float i_alpha_meas;
    float i_beta_meas;

    bool calibrationOK;
    bool overcurrent;
    bool onCommand;
    bool offCommand;
    bool resetCommand;
} globalData;

extern globalData global;

void GD_init(globalData*);

#endif /* GLOBALDATA_GLOBALDATA_H_ */
