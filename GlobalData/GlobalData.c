/*****************************************************************************/
/**
 * @file GlobalData.c
 * @brief GlobalData source file.
 *
 * @author Marco Vinicio (MV)
 *
 * @copyright Universidade Federal de Minas Gerais. All rights reserved
 *****************************************************************************/
#include "GlobalData.h"

/*****************************************************************************/
/**
 * This function initializes a GlobalData instance.
 *
 * @param   self is the GlobalData instance we are working on.
 *
 * @return  None.
 *
 *****************************************************************************/
void GD_init(globalData* self)
{
    self->calibrationOK = false;
    self->offCommand = false;
    self->onCommand = false;
    self->overcurrent = false;
    self->resetCommand = false;

    self->i_alpha_meas = 0;
    self->i_beta_meas = 0;
}
