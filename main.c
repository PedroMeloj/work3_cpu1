//#############################################################################
//
// FILE:   main.c
//
// TITLE:  CPU1 Work 3 - Controller
//
// This is Driverlib and bitfield development for CPU1.
//
//#############################################################################
// $TI Release: F2837xD Support Library v3.12.00.00 $
// $Release Date: Fri Feb 12 19:03:23 IST 2021 $
// $Copyright:
// Copyright (C) 2013-2021 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//#############################################################################

//--- Included Files
#include <math.h>

#include "driverlib.h"
#include "device.h"
#include "F2837xD_device.h"

#include "Configuration/modules_cfg.h"
#include "Application/Application.h"
#include "GlobalData/GlobalData.h"

//--- ISR functions prototype
__interrupt void adcA1ISR(void);
__interrupt void ipc1ISR(void);

//--- Application instance declaration
Application inverter;

//--- GlobalData instance declaration
globalData global;

//--- Allocates duty cycle array d in shared memory
#pragma DATA_SECTION(d,"cpu1tocpu2RAM");
float d[6];

//--- Main
void main(void)
{
    //--- Initialize device clock and peripherals
    Device_init();

    //--- Disable pin locks and enable internal pullups.
    Device_initGPIO();

    //--- Initialize PIE and clear PIE registers. Disables CPU interrupts.
    Interrupt_initModule();

    //--- Initialize the PIE vector table with pointers to the shell Interrupt
    Interrupt_initVectorTable();

    //--- Transfer GlobalSharedRAM blocks ownership to CPU2
    EALLOW;
    MemCfgRegs.GSxMSEL.bit.MSEL_GS12 = 1;
    MemCfgRegs.GSxMSEL.bit.MSEL_GS13 = 1;
    MemCfgRegs.GSxMSEL.bit.MSEL_GS14 = 1;
    MemCfgRegs.GSxMSEL.bit.MSEL_GS15 = 1;
    EDIS;

    //--- Service Routines (ISR).
    Interrupt_register(INT_ADCA1, &adcA1ISR);     // Re-map ADCA1 interrupt signal to call the ISR function
    Interrupt_register(INT_IPC_1, &ipc1ISR);     // Re-map IPC1 interrupt signal to call the ISR function

    //--- Boot CPU2.
#ifdef _STANDALONE
#ifdef _FLASH

    //--- Send boot command to allow the CPU2 application to begin execution
    Device_bootCPU2(C1C2_BROM_BOOTMODE_BOOT_FROM_FLASH);
#else
    //--- Send boot command to allow the CPU2 application to begin execution
    Device_bootCPU2(C1C2_BROM_BOOTMODE_BOOT_FROM_RAM);

#endif // _FLASH
#endif // _STANDALONE

    //--- Initialize GlobalData structur
    GD_init(&global);

    //--- Initialize DACA and DACB
    InitDacA();
    InitDacB();

    //--- Transfer DACA and DACB to CPU2
    EALLOW;
    DevCfgRegs.CPUSEL14.bit.DAC_A = 1;
    DevCfgRegs.CPUSEL14.bit.DAC_B = 1;
    EDIS;

    //--- Initialize ADCA and ADCB
    InitAdcA();
    InitAdcB();

    //--- Configure SOCA conversions;
    InitSOC();

    //--- Initialize ePWM modules.
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);  // Disable the clock to the ePWM modules to have all ePWM modules synchronized
    InitSOCTrigger();                                       // Configure ePWM2 as a 5kHz SOC trigger
    InitHILTrigger();                                       // configure ePWM3 to trigger CPU2 HIL interrupt
    EALLOW;
    DevCfgRegs.CPUSEL0.bit.EPWM3 = 1;                       // Gives ePWM3 control to CPU2
    EDIS;
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);   // TBCLK to ePWM modules enabled

    //--- Initialize Application
    APP_init(&inverter, (float)0.4, (float)50, d);

    //--- Clear IPC Flags
    IpcRegs.IPCCLR.all = 0xFFFFFFFF;

    //--- Wait here until CPU2 is ready
    while (IpcRegs.IPCSTS.bit.IPC17 == 0) ;     // Wait for CPU2 to set IPC17
    IpcRegs.IPCACK.bit.IPC17 = 1;               // Acknowledge and clear IPC17

    //--- Enable interrupts
    Interrupt_enable(INT_ADCA1);
    Interrupt_enable(INT_IPC_1);                 // Enable IPC1 in PIE group 1 and enable INT1 in IER to enable PIE group 1

    //--- Enable Global Interrupt (INTM) and real time interrupt (DBGM)
    EINT;
    ERTM;

    while(1)
    {
        APP_microScope(&inverter);
    }

}

//--- ADC A Interrupt ISR
__interrupt void adcA1ISR(void)
{
    //--- Convert ADC values to floats and store results
    global.i_alpha_meas = ADC_readResult(ADCBRESULT_BASE, ADC_SOC_NUMBER0)/(float)4096*(float)2 - 1;
    global.i_beta_meas = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0)/(float)4096*(float)2 - 1;

    //--- Run application
    APP_fastISR(&inverter);

    //--- Clear the interrupt flag
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

    //--- Check if overflow has occurred
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1))
    {
        ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1);
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
    }

    //--- Acknowledge the interrupt
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

//--- IPC1 Interrupt ISR
__interrupt void ipc1ISR(void)
{

    //--- Acknowledge the interrupt
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);

    //--- Write overcurrent Flag
    global.overcurrent = true;

    //--- Manage the IPC registers
    IpcRegs.IPCACK.bit.IPC1 = 1;                    // Clear IPC0 flag
}

// End of File
