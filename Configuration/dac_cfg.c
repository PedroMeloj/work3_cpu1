/**********************************************************************
* File: dac_cfg.c
* Devices: TMS320F2837xD
* Author: Marco Vinicio
* Description: Functions to configure and initialize DAC modules.
**********************************************************************/

#include "device.h"
#include "driverlib.h"

#include "F2837xD_device.h"


/**********************************************************************
* Function: InitDacA()
*
* Description: Initializes DAC-A for the F2837xD
**********************************************************************/
void InitDacA(void)
{
    //--- Set VREFHI as as the DAC reference voltage
    DAC_setReferenceVoltage(DACA_BASE, DAC_REF_ADC_VREFHI);

    //-- Set load mode
    DAC_setLoadMode(DACA_BASE, DAC_LOAD_SYSCLK);    // Load on next SYSCLK (not using the DAC PWMSYNC signal)

    //--- Set DAC-B output to mid-range
    DAC_setShadowValue(DACA_BASE, 0x0800);          // DACVALS = bits 11-0, bits 15-12 reserved; 0x0800 = 2048

    //--- Enable DAC-B output
    DAC_enableOutput(DACA_BASE);                    // Enable DAC output
    DEVICE_DELAY_US(10);                            // Required delay after enabling the DAC (delay for DAC to power up)

} // end of InitDacb()

/**********************************************************************
* Function: InitDacb()
*
* Description: Initializes DAC-B for the F2837xD
**********************************************************************/
void InitDacB(void)
{
    //--- Set VREFHI as as the DAC reference voltage
    DAC_setReferenceVoltage(DACB_BASE, DAC_REF_ADC_VREFHI);

    //-- Set load mode
    DAC_setLoadMode(DACB_BASE, DAC_LOAD_SYSCLK);    // Load on next SYSCLK (not using the DAC PWMSYNC signal)

    //--- Set DAC-B output to mid-range
    DAC_setShadowValue(DACB_BASE, 0x0800);          // DACVALS = bits 11-0, bits 15-12 reserved; 0x0800 = 2048

    //--- Enable DAC-B output
    DAC_enableOutput(DACB_BASE);                    // Enable DAC output
    DEVICE_DELAY_US(10);                            // Required delay after enabling the DAC (delay for DAC to power up)

} // end of InitDacb()
