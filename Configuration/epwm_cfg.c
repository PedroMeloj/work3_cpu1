/**********************************************************************
* File: epwm_cfg.c
* Devices: TMS320F2837xD
* Author: Marco Vinicio
* Description: Functions to configure and initialize epwm modules.
**********************************************************************/

#include "device.h"
#include "driverlib.h"

/**********************************************************************
* Function: InitSOCTrigger()
*
* Description: Initializes the ePWM2 module on the F2837xD.
*              This module is configured to generate a 5kHz
*              trigger signal for ADC_SOC_A
**********************************************************************/
void InitSOCTrigger(void)
{
    //--- Configure ePWM2 to trigger ADC SOCA at a 5 kHz rate
    SysCtl_resetPeripheral(SYSCTL_PERIPH_RES_EPWM2);    // Reset ePWM2

    EPWM_setTimeBaseCounterMode(EPWM2_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);             // Disable the timer
    EPWM_setClockPrescaler(EPWM2_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);   // TBCLK = EPWMCLK
    EPWM_setSyncOutPulseMode(EPWM2_BASE, EPWM_SYNC_OUT_PULSE_DISABLED);                 // Disable sync-out
    EPWM_setEmulationMode(EPWM2_BASE, EPWM_EMULATION_FREE_RUN);                         // Ignore emulation suspend

    EPWM_setTimeBaseCounter(EPWM2_BASE, 0U);                        // Clear timer counter
    EPWM_setTimeBasePeriod(EPWM2_BASE, 19999U);                     // Set timer period
    EPWM_setPhaseShift(EPWM2_BASE, 0U);                             // Set timer phase
    EPWM_disablePhaseShiftLoad(EPWM2_BASE);                         // Disable phase load

    EPWM_disableADCTrigger(EPWM2_BASE, EPWM_SOC_A);

    EPWM_setADCTriggerSource(EPWM2_BASE, EPWM_SOC_A, EPWM_SOC_TBCTR_PERIOD);     // Set SOCA on PRD event
    EPWM_setADCTriggerEventPrescale(EPWM2_BASE, EPWM_SOC_A, 1);                  // Generate SOCA on first event
    EPWM_enableADCTrigger(EPWM2_BASE, EPWM_SOC_A); // Enable ADC SOCA event

    EPWM_setTimeBaseCounterMode(EPWM2_BASE, EPWM_COUNTER_MODE_UP);  // Enable the timer in count up mode
} // end InitSOCTrigger()

/**********************************************************************
* Function: InitHILTrigger()
*
* Description: Initializes the ePWM3 module on the F2837xD.
*              This module is configured to generate a 5kHz
*              trigger signal for HIL circuit model calculation
**********************************************************************/
void InitHILTrigger(void)
{
    //--- Configure ePWM3 to trigger HIL at a 5 kHz rate
    SysCtl_resetPeripheral(SYSCTL_PERIPH_RES_EPWM3);    // Reset ePWM3

    EPWM_setTimeBaseCounterMode(EPWM3_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);             // Disable the timer
    EPWM_setClockPrescaler(EPWM3_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);   // TBCLK = EPWMCLK
    EPWM_setSyncOutPulseMode(EPWM3_BASE, EPWM_SYNC_OUT_PULSE_DISABLED);                 // Disable sync-out
    EPWM_setEmulationMode(EPWM3_BASE, EPWM_EMULATION_FREE_RUN);                         // Ignore emulation suspend

    EPWM_setTimeBaseCounter(EPWM3_BASE, 10000U);                     // Clear timer counter
    EPWM_setTimeBasePeriod(EPWM3_BASE, 19999U);                     // Set timer period
    EPWM_setPhaseShift(EPWM3_BASE, 0U);                             // Set timer phase
    EPWM_disablePhaseShiftLoad(EPWM3_BASE);                         // Disable phase load

    EPWM_setInterruptSource(EPWM3_BASE, EPWM_INT_TBCTR_PERIOD);
    EPWM_enableInterrupt(EPWM3_BASE);
    EPWM_setInterruptEventCount(EPWM3_BASE, 1U);

    EPWM_setTimeBaseCounterMode(EPWM3_BASE, EPWM_COUNTER_MODE_UP);  // Enable the timer in count up mode
} // end InitHILTrigger()
