/**********************************************************************
* File: soc_cfg.c
* Devices: TMS320F2837xD
* Author: Marco Vinicio
* Description: Functions to configure the Soc conversion
* **********************************************************************/

#include "device.h"
#include "driverlib.h"

/**********************************************************************
* Function: InitSOC()
*
* Description: Initializes SOC_A and configure SOC_A0 conversions.
**********************************************************************/
void InitSOC(void)
{
   // Select the channels to convert and the configure the ePWM trigger
   ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_EPWM2_SOCA,
                ADC_CH_ADCIN4, 14U);
   ADC_setupSOC(ADCB_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_EPWM2_SOCA,
                ADC_CH_ADCIN2, 14U);

   // Select SOC0 on ADCA as the interrupt source.  SOC0 on ADCB will end at
   // the same time, so either SOC2 would be an acceptable interrupt trigger.
   ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER1, ADC_SOC_NUMBER0);
   ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER1);
   ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
}
