/**********************************************************************
* File: modules_cfg.h
* Device: TMS320F2837xD
* Author: Marco Vinicio
* Description: Include file for modules configuration functions.
**********************************************************************/

#ifndef MODULES_CFG_H
#define MODULES_CFG_H

extern void InitDacA(void);
extern void InitDacB(void);
extern void InitAdcA(void);
extern void InitAdcB(void);
extern void InitSOC(void);
extern void InitSOCTrigger(void);
extern void InitHILTrigger(void);


#endif /* MODULES_CFG_H */
