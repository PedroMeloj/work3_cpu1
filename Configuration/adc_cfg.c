/**********************************************************************
* File: adc_cfg.c
* Devices: TMS320F2837xD
* Author: Marco Vinicio
* Description: Functions to configure and initialize ADC modules.
**********************************************************************/

#include "device.h"
#include "driverlib.h"

/**********************************************************************
* Function: InitAdccA()
*
* Description: Initializes ADC-A module on the F2837xD.
*              12bits Single Ended Mode, interrupt at EOC
**********************************************************************/
void InitAdcA(void)
{
    // Set ADCDLK divider to /1
    ADC_setPrescaler(ADCA_BASE, ADC_CLK_DIV_1_0);

    // Set resolution and signal mode (see #defines above) and load
    // corresponding trims.
    ADC_setMode(ADCA_BASE, ADC_RESOLUTION_12BIT, ADC_MODE_SINGLE_ENDED);

    // Set pulse positions to late
    ADC_setInterruptPulseMode(ADCA_BASE, ADC_PULSE_END_OF_CONV);

    // Power up the ADCs and then delay for 1 ms
    ADC_enableConverter(ADCA_BASE);

    // Delay for 1ms to allow ADC time to power up
    DEVICE_DELAY_US(1000);
}

/**********************************************************************
* Function: InitAdcB()
*
* Description: Initializes ADC-A module on the F2837xD.
*              12bits Single Ended Mode.
**********************************************************************/
void InitAdcB(void)
{
    // Set ADCDLK divider to /1
    ADC_setPrescaler(ADCB_BASE, ADC_CLK_DIV_1_0);

    // Set resolution and signal mode (see #defines above) and load
    // corresponding trims.
    ADC_setMode(ADCB_BASE, ADC_RESOLUTION_12BIT, ADC_MODE_SINGLE_ENDED);

    // Set pulse positions to late
    ADC_setInterruptPulseMode(ADCB_BASE, ADC_PULSE_END_OF_CONV);

    // Power up the ADCs and then delay for 1 ms
    ADC_enableConverter(ADCB_BASE);

    // Delay for 1ms to allow ADC time to power up
    DEVICE_DELAY_US(1000);
}
