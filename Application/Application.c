/*****************************************************************************/
/**
 * @file Application.c
 * @brief Application source file.
 *
 * @author Marco Vinicio (MV)
 *
 * @copyright Universidade Federal de Minas Gerais. All rights reserved
 *****************************************************************************/

#include "Application.h"
#include "GlobalData/GlobalData.h"

//--- MicroScope buffers declaration
float bufferAPP[MS_NUMBER_OF_CHANNELS][MS_BUFFER_SIZE];
float displayBufferAPP[MS_NUMBER_OF_CHANNELS][MS_BUFFER_SIZE];

//--- Static function declarations
// CPU2 communication functions
static void APP_connectHIL(Application*);
static void APP_blockHIL(Application*);
static void APP_unblockHIL(Application*);
static void APP_resetHIL(Application*);

// State machine control functions
static void APP_autoStateChange(Application*);
static void APP_transition(Application*);

// State transition fucntions
static void APP_t_INIT_OFF(Application*);
static void APP_t_OFF_ON(Application*);
static void APP_t_ON_OFF(Application*);
static void APP_t_ON_ERROR(Application*);
static void APP_t_ERROR_OFF(Application*);

// Control logic function
static void APP_runControl(Application*);

/*****************************************************************************/
/**
 * This function initializes the application instance.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 *****************************************************************************/
void APP_init(Application* self, const float id_ref, const float f, float* d)
{
    //--- Initialize PI controllers with the parameters defined in Parameter.h
    PI_init(&self->id_loop, CTRL_KP, CTRL_KI, CTRL_KC, CTRL_SAT_NEG, CTRL_SAT_POS, CTRL_TS);
    PI_init(&self->iq_loop, CTRL_KP, CTRL_KI, CTRL_KC, CTRL_SAT_NEG, CTRL_SAT_POS, CTRL_TS);

    //--- Initialize dq measurements
    self->id_meas = 0;
    self->iq_meas = 0;

    //--- Initialize setpoint variables
    self->id_ref = id_ref;
    self->iq_ref = 0;

    //--- Initialize frequency and phase variables
    self->f = f;
    self->wt = 0;

    //--- Initialize pointer to duty cycle array
    self->d = d;

    //--- Initialize all duty cycles as 0(no switching).
    uint16_t i;
    for(i = 0; i < 6; i++){
       self->d[i] = 0;
    }

    //--- Set initial state.
    self->current_state = S_INIT;
    self->next_state = S_INIT;

    //--- Initialize MicroScope
    MS_init(&self->scope);

    //Connect Channels
    MS_connectChannel(&self->scope, 1, &self->id_ref);
    MS_connectChannel(&self->scope, 2, &self->id_meas);

    //Connect buffers
    MS_connectBuffer(&self->scope, 1, (float*)&bufferAPP[0]);
    MS_connectBuffer(&self->scope, 2, (float*)&bufferAPP[1]);

    //Connect display data buffers
    MS_connectOutputBuffer(&self->scope, 1, (float*)&displayBufferAPP[0]);
    MS_connectOutputBuffer(&self->scope, 2, (float*)&displayBufferAPP[1]);

    //Configure trigger
    self->scope.pTrigger = &self->id_ref;
    self->scope.triggerLevel = (float)0.6;
    self->scope.tm = TM_LEVEL_POS;
    self->scope.acq = MS_SINGLE;
    self->scope.state = MS_WAITING;
}

/*****************************************************************************/
/**
 * Run application state machines and control routines.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note Should be called after ADC conversion
 *
 *****************************************************************************/
void APP_fastISR(Application* self)
{
    APP_autoStateChange(self);
    APP_transition(self);
    APP_runControl(self);
}

/*****************************************************************************/
/**
 * Run application scope post processing functions when needed.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note Low priority task. Should be called into main loop.
 *
 *****************************************************************************/
void APP_microScope(Application* self)
{
    MS_main(&self->scope);
}

/*****************************************************************************/
/**
 * Send IPC command to CPU2, sending duty cycle memory address.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note CPU2 communication function.
 *
 *****************************************************************************/
static void APP_connectHIL(Application* self)
{
    IpcRegs.IPCSENDCOM = CMD_CONNECT;
    IpcRegs.IPCSENDADDR =  (uint32_t) self->d;  // Send duty cycle address to CPU2
    IpcRegs.IPCSET.bit.IPC0 = 1;                // Set IPC0 bit for CPU2
}

/*****************************************************************************/
/**
 * Send IPC command to CPU2 to block switching.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note CPU2 communication function.
 *
 *****************************************************************************/
static void APP_blockHIL(Application* self)
{
    IpcRegs.IPCSENDCOM = CMD_BLOCK;
    IpcRegs.IPCSET.bit.IPC0 = 1;                // Set IPC0 bit for CPU2
}

/*****************************************************************************/
/**
 * Send IPC command to CPU2 to start switching.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note CPU2 communication function.
 *
 *****************************************************************************/
static void APP_unblockHIL(Application* self)
{
    IpcRegs.IPCSENDCOM = CMD_UNBLOCK;
    IpcRegs.IPCSET.bit.IPC0 = 1;                // Set IPC0 bit for CPU2
}


/*****************************************************************************/
/**
 * Send IPC command to CPU2 to reset the model.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note CPU2 communication function.
 *
 *****************************************************************************/
static void APP_resetHIL(Application* self)
{
    IpcRegs.IPCSENDCOM = CMD_RESET;
    IpcRegs.IPCSET.bit.IPC0 = 1;                // Set IPC0 bit for CPU2
}

/*****************************************************************************/
/**
 * Verify current state and transition flags determining the next state.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note State Machine Control function.
 *
 *****************************************************************************/
static void APP_autoStateChange(Application* self)
{
    switch(self->current_state)
    {
        case S_INIT:
            if(global.calibrationOK)
            {
                self->next_state = S_OFF;
            }
            break;
        case S_OFF:
            if(global.onCommand)
            {
                self->next_state = S_ON;
            }
            break;
        case S_ON:
            if(global.offCommand)
            {
                self->next_state = S_OFF;
            }else if(global.overcurrent)
            {
                self->next_state = S_ERROR;
            }
            break;
        case S_ERROR:
            if(global.resetCommand)
            {
                self->next_state = S_OFF;
            }
            break;
        default:
            break;
    }
}

/*****************************************************************************/
/**
 * Verify current state and next state. Execute appropriate transition function.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note State Machine Control function.
 *
 *****************************************************************************/
static void APP_transition(Application* self)
{
    if(self->current_state == self->next_state)
    {
        return;
    }

    switch(self->current_state)
    {
        case S_INIT:
            if(self->next_state == S_OFF)
            {
                DINT;
                APP_t_INIT_OFF(self);
                self->current_state = S_OFF;
                EINT;
            }
            break;
        case S_OFF:
            if(self->next_state == S_ON)
            {
                DINT;
                APP_t_OFF_ON(self);
                self->current_state = S_ON;
                EINT;
            }
            break;
        case S_ON:
            if(self->next_state == S_ERROR)
            {
                DINT;
                APP_t_ON_ERROR(self);
                self->current_state = S_ERROR;
                EINT;
            }else if(self->next_state == S_OFF)
            {
                DINT;
                APP_t_ON_OFF(self);
                self->current_state = S_OFF;
                EINT;
            }
            break;
        case S_ERROR:
            if(self->next_state == S_OFF)
            {
                DINT;
                APP_t_ERROR_OFF(self);
                self->current_state = S_OFF;
                EINT;
            }
            break;
        default:
            break;
    }
}

/*****************************************************************************/
/**
 * S_INIT to S_OFF transition routine.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note State transition function.
 *
 *****************************************************************************/
static void APP_t_INIT_OFF(Application* self)
{
    APP_connectHIL(self);
}

/*****************************************************************************/
/**
 * S_OFF to S_ON transition routine.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note State transition function.
 *
 *****************************************************************************/
static void APP_t_OFF_ON(Application* self)
{
    //--- Enable control loops
    PI_enable(&self->id_loop);
    PI_enable(&self->iq_loop);

    //--- Unblock HIL
    APP_unblockHIL(self);
}

/*****************************************************************************/
/**
 * S_ON to S_ERROR transition routine.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note State transition function.
 *
 *****************************************************************************/
static void APP_t_ON_ERROR(Application* self)
{
    //--- Disable control loops
    PI_disable(&self->id_loop);
    PI_disable(&self->iq_loop);

    //--- Write 0 to duty cycles
    self->d[0] = 0;
    self->d[1] = 0;
    self->d[2] = 0;
    self->d[3] = 0;
    self->d[4] = 0;
    self->d[5] = 0;
}

/*****************************************************************************/
/**
 * S_ON to S_OFF transition routine.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note State transition function.
 *
 *****************************************************************************/
static void APP_t_ON_OFF(Application* self)
{
    //--- Disable control loops
    PI_disable(&self->id_loop);
    PI_disable(&self->iq_loop);

    //--- Write 0 to duty cycles
    self->d[0] = 0;
    self->d[1] = 0;
    self->d[2] = 0;
    self->d[3] = 0;
    self->d[4] = 0;
    self->d[5] = 0;

    //--- block HIL
    APP_blockHIL(self);
}

/*****************************************************************************/
/**
 * S_ERROR to S_OFF transition routine.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note State transition function.
 *
 *****************************************************************************/
static void APP_t_ERROR_OFF(Application* self)
{
   //--- Reset control loops
   PI_reset(&self->id_loop);
   PI_reset(&self->iq_loop);

   //--- reset HIL
   APP_resetHIL(self);
}

/*****************************************************************************/
/**
 * Run control logic when in S_ON state.
 *
 * @param   self is the application instance we are working on.
 *
 * @return  None.
 *
 * @note Control logic function.
 *
 *****************************************************************************/
static void APP_runControl(Application* self)
{
    if(self->current_state == S_ON)
    {
        float mod_alpha, mod_beta;  // PWM modulation waveform in alpha beta coordinates;
        float mod_a, mod_b, mod_c;  // PWM modulation waveforms in abc coordinates;

        //--- Update phase reference
        self->wt = self->wt + self->f*2*M_PI*CTRL_TS;  // Phase Reference
        while(self->wt > 2*M_PI)
        {
            // Resets Phase Reference after 2pi
            self->wt = self->wt - 2*M_PI;
        }

        //--- Alpha-beta to dq transform
        float k1 = -sinf(self->wt);
        float k2 = cosf(self->wt);
        self->id_meas = global.i_alpha_meas*k2 - global.i_beta_meas*k1;
        self->iq_meas = global.i_alpha_meas*k1 + global.i_beta_meas*k2;

        //--- Run control loops
        self->id_loop.feedback = self->id_meas;
        self->id_loop.setpoint = self->id_ref;
        PI_run(&self->id_loop);

        self->iq_loop.feedback = self->iq_meas;
        self->iq_loop.setpoint = self->iq_ref;
        PI_run(&self->iq_loop);

        //--- Dq to alpha beta transform
        mod_alpha = self->id_loop.output*k2 + self->iq_loop.output*k1;
        mod_beta = - self->id_loop.output*k1 + self->iq_loop.output*k2;

        //--- Alpha beta to abc transform
        mod_b = -mod_alpha/(float)2;
        mod_c = mod_b - mod_beta*sqrtf(3)/(float)2;
        mod_b = mod_b + mod_beta*sqrtf(3)/(float)2;
        mod_a = mod_alpha;

        //--- Duty cycle calculation
        self->d[0] = (mod_a + 1)/(float)2;
        self->d[1] = 1 - self->d[0];
        self->d[2] = (mod_b + 1)/(float)2;
        self->d[3] = 1 - self->d[2];
        self->d[4] = (mod_c + 1)/(float)2;
        self->d[5] = 1 - self->d[4];

        //--- Save data to scope
        MS_saveData(&self->scope);
    }
}
