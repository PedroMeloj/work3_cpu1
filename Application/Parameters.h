/*****************************************************************************/
/**
 * @file Parameters.h
 * @brief Define some parameters for model circuits and controllers.
 *
 * @author Marco Vinicio (MV)
 *
 * @copyright Universidade Federal de Minas Gerais. All rights reserved
 *****************************************************************************/

#ifndef APPLICATION_PARAMETERS_H_
#define APPLICATION_PARAMETERS_H_

#define CTRL_KP (float)0.5
#define CTRL_KI (float)500
#define CTRL_KC (float)1
#define CTRL_SAT_POS (float)0.707
#define CTRL_SAT_NEG (float)-0.707
#define CTRL_TS (float)0.0002

#define CIR_R (float)2
#define CIR_L (float)0.01
#define CIR_E (float)48

#define HIL_IBASE (float)7.5
#define HIL_ITRIP (float)10
#define HIL_TS (float)0.0002

#endif /* APPLICATION_PARAMETERS_H_ */
