/*****************************************************************************/
/**
 * @file Application.h
 * @brief Application header file.
 *
 * @author Marco Vinicio (MV)
 *
 * @copyright Universidade Federal de Minas Gerais. All rights reserved
 *****************************************************************************/

#ifndef APPLICATION_APPLICATION_H_
#define APPLICATION_APPLICATION_H_

#include <stdint.h>
#include <math.h>

#include "driverlib.h"
#include "device.h"
#include "F2837xD_device.h"

#include "PiController/Picontroller.h"
#include "GlobalData/GlobalData.h"
#include "MicroScope/MicroScope.h"
#include "Parameters.h"

typedef enum{
    S_INIT,
    S_OFF,
    S_ON,
    S_ERROR
} State;

typedef enum{
    CMD_CONNECT = 0x01U,
    CMD_UNBLOCK = 0x02U,
    CMD_BLOCK = 0x03U,
    CMD_RESET = 0x04U
} IpcCommands;

typedef struct {
    State current_state;                //state machine current state
    State next_state;                   //state machine next state

    PiController id_loop;               //direct current control loop
    PiController iq_loop;               //quadrature current control loop

    float id_meas, iq_meas;             // Measured current in dq coordinates
    float id_ref, iq_ref;               // Control loop setpoint currents

    float f;                            // Output frequency
    float wt;

    float* d;                           //Duty cycle vector that will be read by CPU2

    MicroScope scope;
} Application;

void APP_init(Application*, const float, const float, float*);
void APP_fastISR(Application*);
void APP_microScope(Application*);

#endif /* APPLICATION_APPLICATION_H_ */
